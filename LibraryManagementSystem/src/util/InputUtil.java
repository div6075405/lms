package util;

import java.util.Scanner;

public class InputUtil {
    public static String InputRequiredString(String title){
        Scanner input = new Scanner(System.in);
        System.out.print(title);
        return input.nextLine();
    }
    public static Double InputRequiredDouble(String title){
        Scanner input = new Scanner(System.in);
        System.out.print(title);
        return input.nextDouble();
    }

    public static long InputRequiredLong(String title){
        Scanner input = new Scanner(System.in);
        System.out.print(title);
        return input.nextLong();
    }

    public static int InputRequiredInteger(String title){
        Scanner input = new Scanner(System.in);
        System.out.print(title);

        return input.nextInt();
    }
}
