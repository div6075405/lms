package service;

import global.Data;
import model.Book;
import util.InputUtil;


import static util.InputUtil.*;


public class LibraryServiceImpl implements LibraryService{

    public Book fillBooks(){
        String bookName = InputRequiredString("Enter the book name");
        String author = InputRequiredString("Enter the author");
        String genre = InputRequiredString("Enter the genre");
        String language = InputRequiredString("Enter the language");
        Long pageCount = InputRequiredLong("Enter the page count");
        Double prize = InputRequiredDouble("Enter the prize");
        Integer count = InputRequiredInteger("Enter the count");
        Integer stock = InputRequiredInteger("Enter the status stock");
        Book book = new Book(prize , count , stock , bookName, author, genre , pageCount , language);
        return  book;
    }




    @Override
    public boolean register() {
        int count =InputRequiredInteger("How much register books: ");
        Data.books = new Book[count];
        if (Data.books !=null){
            Book [] tempBook = Data.books;
            Data.books= new Book[count+tempBook.length];

            for (int i = 0, k=0; i < Data.books.length; i++) {
                if(i< tempBook.length){
                    Data.books[i]=tempBook[i];
                }else {
                    System.out.println("-------------------");
                    System.out.print(k+1 + ". Book");
                    Data.books[i] = fillBooks();
                    System.out.println("-------------------");
                }

            }

        } else {
            Data.books = new Book[count];
            for(int i = 0; i<count; i++){
                System.out.println("-------------------");
                System.out.print(i+1 + ". Book");
                Data.books[i] = fillBooks();
                System.out.println("-------------------");
            }
            System.out.println("Total books: " + Data.books.length);
        }





        return true;
    }

    @Override
    public boolean update() {

        return false;
    }

    @Override
    public boolean delete() {
        boolean isDeleted = false;

        if(Data.books == null){
            System.err.println("Book doesnt exist!");
            return isDeleted;
        } else {
            long id = InputRequiredLong("Which is update book: ");
            for (Book book: Data.books) {
                if(book.getId()==id){
                    Book [] tempBooks = Data.books;
                    Data.books= new Book[tempBooks.length-1];
                    int k=0;
                    for (Book book1: tempBooks) {
                        if(book1.getId()==id) {
                            continue;
                        }

                        k++;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void findByName() {
        String name = InputRequiredString("name: ");
        for (int i = 0, k = 0; i < Data.books.length; i++) {

            if (Data.books[i].getBookName().contains(name)) {
                System.out.print(++k + ". Book");
                System.out.println(Data.books[i]);
                System.out.println("------------------------");
            }

        }


    }

    @Override
    public String totalBooks() {
        if (Data.books==null){
            System.out.println("Book doesnt exist!");
        }
        return "Book list count: " + Data.books.length;

    }
}
