package model;

public class Magazine {
    private Double prize;
    private long count;
    private int stock=1;

    public Magazine() {
    }

    public Magazine(Double prize, long count, int stock) {
        this.prize = prize;
        this.count = count;
        this.stock = stock;
    }

    public Double getPrize() {
        return prize;
    }

    public void setPrize(Double prize) {
        this.prize = prize;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "prize=" + prize +
                ", count=" + count +
                ", stock=" + stock +
                '}';
    }
}
