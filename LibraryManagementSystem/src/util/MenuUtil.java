package util;

import static util.InputUtil.InputRequiredInteger;

public class MenuUtil {
    public static int entryApp() {
        System.out.println("----------| Library Management System |-------------" +
                "\n[0] Exit system" +
                "\n[1] Register" +
                "\n[2] Show books" +
                "\n[3] Update" +
                "\n[4] Delete" +
                "\n[5] Total books " +

                "\n[6] Find by name");

        return InputRequiredInteger("Choose option: ");
    }
}
