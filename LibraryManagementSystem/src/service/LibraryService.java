package service;

public interface LibraryService {
    boolean register();
    boolean update();
    boolean delete();
    void findByName();
    String totalBooks();

}
