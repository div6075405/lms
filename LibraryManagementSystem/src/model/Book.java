package model;

public class Book extends Magazine{
    private static long id;
    private String bookName;
    private String author;
    private String genre;
    private long pageCount;
    private String language;

    public Book(){
        ++this.id;
    }

    public static long getId() {
        return id;
    }

    public static void setId(long id) {
        Book.id = id;
    }

    public Book(Double prize, long count, int stock, String bookName, String author, String genre, long pageCount, String language) {
        super(prize, count, stock);
        this.bookName = bookName;
        this.author = author;
        this.genre = genre;
        this.pageCount = pageCount;
        this.language = language;
    }



    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public long getPageCount() {
        return pageCount;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookName='" + bookName + '\'' +
                ", author='" + author + '\'' +
                ", genre='" + genre + '\'' +
                ", pageCount=" + pageCount +
                ", language='" + language + '\'' +
                '}';
    }
}

